package com.aldebaran.env_provider.service.impl;

import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.creation.FlavorCreateDTO;
import com.aldebaran.env_provider.mapper.FlavorMapper;
import com.aldebaran.env_provider.model.Flavor;
import com.aldebaran.env_provider.service.IFlavorService;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Represents the service layer for the Flavor entity.
 *
 * @author Tarasov Ivan
 */

@ApplicationScoped
public class FlavorService implements IFlavorService {

  @Inject
  FlavorMapper mapper;

  @Override
  public FlavorDTO add(FlavorCreateDTO dto) {

    Flavor flavor = new Flavor();

    flavor.name = dto.getName();
    flavor.mountPoint = dto.getMountPoint();
    flavor.size = dto.getSize();

    flavor.persist();

    return mapper.toDTO(flavor);
  }

  @Override
  public boolean delete(UUID id) {
    Flavor flavor = Flavor.findById(id);

    if (flavor != null) {
      flavor.delete();
      return true;
    }
    return false;
  }

  @Override
  public void deleteAll() {
    Flavor.deleteAll();
  }

  @Override
  public FlavorDTO update(UUID id, FlavorCreateDTO dto) {

    Flavor flavor = Flavor.findById(id);

    if (flavor != null) {
      flavor.name = dto.getName();
      flavor.mountPoint = dto.getMountPoint();
      flavor.size = dto.getSize();

      flavor.persistAndFlush();

      return mapper.toDTO(flavor);
    }

    return null;
  }

  @Override
  public FlavorDTO get(UUID id) {
    return mapper.toDTO(Flavor.findById(id));
  }

  @Override
  public List<FlavorDTO> list() {
    return mapper.toDTOList(Flavor.findAll().list());
  }
}
