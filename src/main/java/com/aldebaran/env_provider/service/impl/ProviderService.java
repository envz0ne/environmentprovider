package com.aldebaran.env_provider.service.impl;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.ProviderConnectionDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.creation.ProviderCreateDTO;
import com.aldebaran.env_provider.mapper.EnvironmentMapper;
import com.aldebaran.env_provider.mapper.ProviderMapper;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.Provider;
import com.aldebaran.env_provider.service.IProviderService;
import com.aldebaran.env_provider.util.encryption.PasswordUtils;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.UriInfo;

/**
 * Represents the service layer for the Provider entity.
 *
 * @author Tarasov Ivan
 */

@ApplicationScoped
public class ProviderService implements IProviderService {

  @Inject
  ProviderMapper mapper;

  @Inject
  EnvironmentMapper envMapper;

  @Override
  public ProviderDTO add(ProviderCreateDTO dto) {

    Provider provider = new Provider();

    // Environmet with this name is already exists
    if (Provider.find("name", dto.getName()).firstResult() != null) {
      return null;
    }

    provider.userId = dto.getUserId();
    provider.name = dto.getName();
    provider.url = dto.getUrl();
    provider.login = dto.getLogin();
    provider.type = dto.getType();
    provider.connectionType = dto.getConnectionType();

    String salt = PasswordUtils.getSalt(30);

    provider.password = PasswordUtils
        .generatePassword(dto.getPassword(), salt);
    provider.passwordSalt = salt;

    provider.persistAndFlush();

    return mapper.toDTO(provider);
  }

  @Override
  public ProviderDTO addEnvironment(UUID id, EntityIdDTO env) {

    Provider provider = Provider.findById(id);
    Environment environment = Environment.findById(env.getId());

    if (provider != null && environment != null) {
      provider.environments.add(environment);
      provider.persistAndFlush();

      return mapper.toDTO(provider);
    }

    return null;
  }

  @Override
  public boolean delete(UUID id) {
    Provider provider = Provider.findById(id);

    if (provider != null) {
      provider.delete();
      return true;
    }
    return false;
  }

  @Override
  public void deleteAll() {
    Provider.deleteAll();
  }

  @Override
  public boolean deleteEnvironments(UUID id) {
    Provider provider = Provider.findById(id);

    if (provider != null) {
      provider.environments = null;
      provider.persistAndFlush();
      return true;
    }
    return false;

  }

  @Override
  public ProviderDTO update(UUID id, ProviderCreateDTO dto) {
    Provider provider = Provider.findById(id);

    if (provider != null) {

      provider.name = dto.getName();
      provider.url = dto.getUrl();

      provider.login = dto.getLogin();

      String salt = PasswordUtils.getSalt(30);

      provider.password = PasswordUtils
          .generatePassword(dto.getPassword(), salt);
      provider.passwordSalt = salt;

      provider.persistAndFlush();
    }

    return mapper.toDTO(provider);
  }

  @Override
  public ProviderDTO get(UUID id) {

    Provider provider = Provider.findById(id);

    if (provider != null) {
      return mapper.toDTO(provider);
    }

    return null;
  }

  @Override
  public List<EnvironmentDTO> getEnvironments(UUID id) {

    Provider provider = Provider.findById(id);

    if (provider != null) {
      return envMapper.toDTOList(provider.environments);
    }

    return null;
  }

  @Override
  public List<ProviderDTO> list() {
    return mapper.toDTOList(Provider.listAll());
  }

  @Override
  public List<ProviderConnectionDTO> listPages(UriInfo info) {

    PanacheQuery<Provider> listPage = Provider
        .find("user_id",
            UUID.fromString(info.getQueryParameters().getFirst("user_id"))
        );

    Page page = new Page(
        Integer.parseInt(info.getQueryParameters().getFirst("index")),
        Integer.parseInt(info.getQueryParameters().getFirst("size"))
    );
    listPage.page(page);

    return mapper.toConnectionDTOList(listPage.list());
  }
}