package com.aldebaran.env_provider.service.impl;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.creation.HostCreateDTO;
import com.aldebaran.env_provider.mapper.FlavorMapper;
import com.aldebaran.env_provider.mapper.HostMapper;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.Flavor;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.service.IHostService;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Represents the service layer for the Host entity.
 *
 * @author Tarasov Ivan
 */
@ApplicationScoped
public class HostService implements IHostService {

  @Inject
  HostMapper mapper;

  @Inject
  FlavorMapper flavorMapper;

  @Override
  public HostDTO add(HostCreateDTO dto) {

    Host host = new Host();
    host.name = dto.getName();
    host.url = dto.getUrl();
    host.image = Image.findById(dto.getImageId());
    host.environment = Environment.findById(dto.getEnvId());
    host.ram = dto.getRam();
    host.vcpu = dto.getVcpu();

    host.persistAndFlush();

    return mapper.toDTO(host);
  }

  @Override
  public HostDTO addFlavor(UUID id, EntityIdDTO flavorId) {

    Host host = Host.findById(id);
    Flavor flavor = Flavor.findById(flavorId.getId());

    if (host != null && flavor != null) {
      host.flavors.add(flavor);
      host.persist();

      flavor.host = host;
      flavor.persistAndFlush();

      return mapper.toDTO(host);
    }
    return null;
  }

  @Override
  public boolean delete(UUID id) {

    Host host = Host.findById(id);

    if (host != null) {
      host.delete();
      return true;
    }
    return false;
  }

  @Override
  public void deleteAll() {
    Host.deleteAll();
  }

  @Override
  public boolean deleteFlavorList(UUID id) {

    Host host = Host.findById(id);

    if (host != null) {
      host.flavors = null;
      host.persistAndFlush();
      return true;
    }
    return false;
  }

  @Override
  public HostDTO update(UUID id, HostCreateDTO hostUpdate) {
    Host host = Host.findById(id);

    if (host != null) {
      host.name = hostUpdate.getName();

      Image image = Image.findById(hostUpdate.getImageId());

      if (image != null) {
        host.image = image;
      }

      Environment environment = Environment.findById(hostUpdate.getEnvId());

      if (environment != null) {
        host.environment = environment;
      }

      host.url = hostUpdate.getUrl();
      host.ram = hostUpdate.getRam();
      host.vcpu = hostUpdate.getVcpu();

      host.persistAndFlush();
    }
    return mapper.toDTO(host);
  }

  @Override
  public HostDTO get(UUID id) {
    return mapper.toDTO(Host.findById(id));
  }

  @Override
  public List<HostDTO> list() {
    return mapper.toDTOList(Host.listAll());
  }

  @Override
  public List<FlavorDTO> flavorList(UUID id) {

    Host host = Host.findById(id);

    if (host != null) {
      return flavorMapper.toDTOList(host.flavors);
    }

    return null;
  }

  @Override
  public Image getImageById(UUID id) {
    Host host = Host.findById(id);

    if (host != null) {
      return host.image;
    }

    return null;
  }

  @Override
  public boolean deleteImage(UUID id) {

    Host host = Host.findById(id);

    if (host != null) {
      host.image = null;
      host.persistAndFlush();
      return true;
    }
    return false;
  }

  @Override
  public UUID getEnvironment(UUID id) {

    Host host = Host.findById(id);

    if (host != null) {
      return host.environment.id;
    }

    return null;
  }

  @Override
  public HostDTO addEnvironment(UUID id, EntityIdDTO envId) {

    Host host = Host.findById(id);
    Environment environment = Environment.findById(envId.getId());

    if (host != null && environment != null) {
      host.environment = environment;
      host.persistAndFlush();

      return mapper.toDTO(host);
    }

    return null;
  }

  @Override
  public boolean deleteEnvironment(UUID id) {
    Host host = Host.findById(id);

    if (host != null) {
      host.environment = null;
      host.persistAndFlush();
      return true;
    }
    return false;
  }
}