package com.aldebaran.env_provider.service.impl;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.EnvironmentInstanceDTO;
import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.ImageDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.creation.EnvFullCreationDTO;
import com.aldebaran.env_provider.dto.creation.EnvironmentCreateDTO;
import com.aldebaran.env_provider.dto.creation.FlavorCreateDTO;
import com.aldebaran.env_provider.dto.creation.HostCreateDTO;
import com.aldebaran.env_provider.dto.creation.ProviderCreateDTO;
import com.aldebaran.env_provider.mapper.EnvironmentMapper;
import com.aldebaran.env_provider.mapper.FlavorMapper;
import com.aldebaran.env_provider.mapper.HostInstanceMapper;
import com.aldebaran.env_provider.mapper.HostMapper;
import com.aldebaran.env_provider.mapper.ProviderMapper;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.EnvironmentInstance;
import com.aldebaran.env_provider.model.Flavor;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.model.Provider;
import com.aldebaran.env_provider.service.IEnvironmentService;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.UriInfo;

/**
 * Represents the service layer for the Environment entity.
 *
 * @author Tarasov Ivan
 */

@ApplicationScoped
public class EnvironmentService implements IEnvironmentService {

  @Inject
  EnvironmentMapper mapper;

  @Inject
  ProviderMapper providerMapper;

  @Inject
  FlavorMapper flavorMapper;

  @Inject
  HostMapper hostMapper;

  @Inject
  HostInstanceMapper hostInstanceMapper;

  @Inject
  HostService hostService;

  @Inject
  ProviderService providerService;

  @Inject
  FlavorService flavorService;

  @Inject
  ImageService imageService;

  @Override
  public EnvironmentDTO add(EnvironmentCreateDTO dto) {

    Environment env = new Environment();

    // Environmet with this name is already exists
    if (Environment.find("name", dto.getName()).firstResult() != null) {
      return null;
    }

    env.name = dto.getName();

    Provider provider = Provider.findById(dto.getProviderId());

    if (provider != null) {
      env.provider = provider;
    }

    env.persist();

    return mapper.toDTO(env);
  }

  @Override
  public EnvironmentDTO addFull(EnvFullCreationDTO dto) {

    EnvironmentDTO res = new EnvironmentDTO();

    // If the env. with the same exists, then return null.
    if (Environment.find("name", dto.getName()).firstResult() != null) {
      return null;
    }

    Environment env = new Environment();
    env.userId = dto.getUserId();
    env.name = dto.getName();

    // if chose existed provider
    Provider provider = Provider.findById(dto.getProvider().getId());
    if (provider == null) {
      ProviderCreateDTO providerCreateDTO = new ProviderCreateDTO();

      providerCreateDTO.setUserId(dto.getUserId());
      providerCreateDTO.setName(dto.getProvider().name);
      providerCreateDTO.setType(dto.getProvider().type);
      providerCreateDTO.setConnectionType(dto.getProvider().connectionType);
      providerCreateDTO.setLogin(dto.getProvider().getLogin());
      providerCreateDTO.setUrl(dto.getProvider().getUrl());
      providerCreateDTO.setPassword(dto.getProvider().getPassword());

      provider = providerMapper.toEntity(providerService.add(providerCreateDTO));
    }

    env.provider = provider;

    env.persist();

    return setEnvironmentHostFromDTO(dto, env);
  }

  @Override
  public EnvironmentDTO get(UUID id) {
    return mapper.toDTO(Environment.findById(id));
  }

  @Override
  public EnvironmentDTO update(UUID id, EnvFullCreationDTO dto) {

    Environment env = Environment.findById(id);
    if (env != null) {
      // Set the new name
      env.name = dto.getName();
      // Set the new provider
      env.provider = Provider.findById(dto.getProvider().getId());
      // Set the new hosts

      List<Host> hosts = env.hosts;
      List<HostDTO> newHosts = dto.getHosts();
      hosts.forEach(
          originalHost -> {
            newHosts.forEach(
                newHost -> {
                  // If host dto == host object
                  if (originalHost.id.compareTo(newHost.getId()) == 0) {
                    // Set metadata
                    System.out.println(originalHost.name);
                    originalHost.name = newHost.getName();
                    System.out.println(newHost.getName());
                    originalHost.ram = newHost.getRam();
                    originalHost.vcpu = newHost.getVcpu();
                    // Set the new image
                    originalHost.image = newHost.getImage();
                    originalHost.persist();

                    // Set the new flavors
                    if (newHost.getFlavors().isEmpty()) {
                      System.out.println("Delete flavors");
                      originalHost.flavors.forEach(PanacheEntityBase::delete);
                    } else {
                      // flavors are empty
                      if (originalHost.flavors.isEmpty()) {
                        // add for the empty flavores
                        for (FlavorDTO f : newHost.getFlavors()) {
                          FlavorCreateDTO flavorCreateDTO = new FlavorCreateDTO();
                          flavorCreateDTO.setName(f.getName());
                          flavorCreateDTO.setMountPoint(f.getMountPoint());
                          flavorCreateDTO.setSize(f.getSize());

                          Flavor flavor = flavorMapper.toEntity(flavorService.add(flavorCreateDTO));

                          EntityIdDTO flavorId = new EntityIdDTO();
                          flavorId.setId(flavor.id);
                          hostService.addFlavor(originalHost.id, flavorId);
                        }
                      } else {
                        // add new flavores
                        if (originalHost.flavors.size() < newHost.getFlavors().size()) {
                          List<FlavorDTO> newFlavores = new ArrayList<>(newHost.getFlavors());

                          for (FlavorDTO f : newHost.getFlavors()) {
                            if (f.getId() != null) {
                              newFlavores.remove(f);
                            }
                          }

                          for (FlavorDTO f : newFlavores) {
                            FlavorCreateDTO flavorCreateDTO = new FlavorCreateDTO();
                            flavorCreateDTO.setName(f.getName());
                            flavorCreateDTO.setMountPoint(f.getMountPoint());
                            flavorCreateDTO.setSize(f.getSize());

                            Flavor flavor = flavorMapper.toEntity(flavorService.add(flavorCreateDTO));

                            EntityIdDTO flavorId = new EntityIdDTO();
                            flavorId.setId(flavor.id);
                            hostService.addFlavor(originalHost.id, flavorId);
                          }
                        } else {
                          // rm one or more flavores? but not all
                          if (originalHost.flavors.size() > newHost.getFlavors().size()) {
                            List<Flavor> origianlFlavors = originalHost.flavors;
                            origianlFlavors
                                .forEach(
                                    o -> {
                                      AtomicBoolean removed = new AtomicBoolean(true);

                                      newHost.getFlavors()
                                          .forEach(
                                              nf -> {
                                                if (o.id.compareTo(nf.getId()) == 0) {
                                                  removed.set(false);
                                                }
                                              }
                                          );

                                      if (removed.get()) {
                                        o.delete();
                                      }
                                    }
                                );
                          } else {
                            originalHost.flavors
                                .forEach(
                                    originalFlavor -> {
                                      newHost.getFlavors()
                                          .forEach(
                                              newFlavor -> {
                                                if (originalFlavor.id.compareTo(newFlavor.getId()) == 0) {
                                                  System.out.println(originalFlavor.id);
                                                  originalFlavor.name = newFlavor.getName();
                                                  originalFlavor.size = newFlavor.getSize();
                                                  originalFlavor.mountPoint = newFlavor.getMountPoint();
                                                  // Save the updated metadata for the flavor
                                                  originalFlavor.persistAndFlush();
                                                }
                                              }
                                          );
                                    }
                                );
                          }
                        }
                      }
                    }
                    // Save the updated metadata for the host
                    originalHost.persistAndFlush();
                  }
                }
            );
          }
      );
      // Save the updated metadata for the environment
      env.persistAndFlush();
      return mapper.toDTO(env);
    }
    return null;
  }

  @Override
  public boolean delete(UUID id) {

    Environment env = Environment.findById(id);
    if (env != null) {

      env.hosts.forEach(o -> o.flavors.forEach(PanacheEntityBase::delete));
      env.hosts.forEach(PanacheEntityBase::delete);

      env.delete();

      return true;
    }
    return false;
  }

  @Override
  public void deleteAll() {
    Environment.deleteAll();
  }

  private EnvironmentDTO setEnvironmentHostFromDTO(EnvFullCreationDTO dto, Environment env) {
    for (HostDTO h : dto.getHosts()) {

      HostCreateDTO hostCreateDTO = new HostCreateDTO();

      hostCreateDTO.setEnvId(env.id);
      hostCreateDTO.setName(h.getName());
      hostCreateDTO.setRam(2);
      hostCreateDTO.setVcpu(2);

      if (h.getImage().id == null) {

        Image image = new Image();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setUserId(dto.getUserId());
        imageDTO.setName(h.getImage().name);
        imageDTO.setTag(h.getImage().tag);
        imageDTO.setPath(h.getImage().path);

        image = imageService.add(imageDTO);
        hostCreateDTO.setImageId(image.id);
      } else {
        hostCreateDTO.setImageId(h.getImage().id);
      }

      Host createdHost = hostMapper.toEntity(hostService.add(hostCreateDTO));
      // Add flavor
      for (FlavorDTO f : h.getFlavors()) {
        FlavorCreateDTO flavorCreateDTO = new FlavorCreateDTO();
        flavorCreateDTO.setName(f.getName());
        flavorCreateDTO.setMountPoint(f.getMountPoint());
        flavorCreateDTO.setSize(f.getSize());

        Flavor flavor = flavorMapper.toEntity(flavorService.add(flavorCreateDTO));

        EntityIdDTO flavorId = new EntityIdDTO();
        flavorId.setId(flavor.id);
        hostService.addFlavor(createdHost.id, flavorId);
      }
    }

    env.persistAndFlush();
    return mapper.toDTO(env);
  }

  @Override
  public EnvironmentDTO addProvider(UUID id, EntityIdDTO providerId) {
    Environment env = Environment.findById(id);
    Provider provider = Provider.findById(providerId.getId());

    if (env != null && provider != null) {
      env.provider = provider;
      env.persistAndFlush();

      return mapper.toDTO(env);
    }
    return null;
  }

  @Override
  public EnvironmentDTO addHost(UUID id, EntityIdDTO hostId) {

    Environment env = Environment.findById(id);
    Host host = Host.findById(hostId.getId());

    if (env != null && host != null) {
      env.hosts.add(host);
      env.persistAndFlush();

      return mapper.toDTO(env);
    }

    return null;
  }

  @Override
  public boolean removeProvider(UUID id) {

    Environment env = Environment.findById(id);
    if (env != null) {
      env.provider = null;
      env.persist();
      return true;
    }
    return false;
  }

  @Override
  public boolean deleteHosts(UUID id) {

    Environment env = Environment.findById(id);
    if (env != null) {
      env.hosts.forEach(PanacheEntityBase::delete);
      return true;
    }
    return false;
  }

  @Override
  public ProviderDTO getProvider(UUID id) {
    Environment env = Environment.findById(id);

    if (env != null) {
      return providerMapper.toDTO(env.provider);
    }
    return null;
  }

  @Override
  public List<EnvironmentDTO> list() {
    return mapper.toDTOList(Environment.listAll());
  }

  @Override
  public List<EnvironmentDTO> listPages(UriInfo uriInfo) {

    PanacheQuery<Environment> listPage =
        Environment
            .find("user_id",
                UUID.fromString(uriInfo.getQueryParameters().getFirst("user_id"))
            );

    Page page = new Page(
        Integer.parseInt(uriInfo.getQueryParameters().getFirst("index")),
        Integer.parseInt(uriInfo.getQueryParameters().getFirst("size"))
    );
    listPage.page(page);

    return mapper.toDTOList(listPage.list());
  }

  @Override
  public List<EnvironmentInstanceDTO> listEnvInstancePages(UriInfo uriInfo) {

    PanacheQuery<EnvironmentInstance> listPage =
        EnvironmentInstance
            .find("user_id",
                UUID.fromString(uriInfo.getQueryParameters().getFirst("user_id"))
            );

    Page page = new Page(
        Integer.parseInt(uriInfo.getQueryParameters().getFirst("index")),
        Integer.parseInt(uriInfo.getQueryParameters().getFirst("size"))
    );
    listPage.page(page);

    return mapper.toEnvInstanceDTOList(listPage.list());
  }

  @Override
  public List<HostDTO> hostList(UUID id) {

    Environment env = Environment.findById(id);
    if (env != null) {
      return hostMapper.toDTOList(env.hosts);
    }

    return null;
  }

  @Override
  public EnvironmentInstanceDTO getEnvInstance(UUID envInstanceId) {

    EnvironmentInstance envInstance = EnvironmentInstance.findById(envInstanceId);

    if (envInstance == null) {
      return null;
    }

    EnvironmentInstanceDTO dto = new EnvironmentInstanceDTO();
    try {
      dto.setId(envInstanceId);
      dto.setEnvId(envInstance.environment.id);
      dto.setHosts(hostInstanceMapper.toDTOList(envInstance.hostInstances));
      dto.setProvider(providerMapper.toConectionDTO(envInstance.environment.provider));
    } catch (Exception exp) {
      exp.printStackTrace();
    }

    return dto;
  }

  @Override
  public List<EnvironmentInstanceDTO> listEnvInstances() {
    return mapper.toEnvInstanceDTOList(EnvironmentInstance.findAll().list());
  }

  @Override
  public List<EnvironmentInstanceDTO> listEnvInstancesForUser(UUID userId) {
    return mapper.toEnvInstanceDTOList(EnvironmentInstance.find("user_id", userId).list());
  }
}
