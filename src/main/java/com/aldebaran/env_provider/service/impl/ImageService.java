package com.aldebaran.env_provider.service.impl;

import com.aldebaran.env_provider.dto.ImageDTO;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.service.IImageService;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.UriInfo;

/**
 * Represents the service layer for the Image entity.
 *
 * @author Tarasov Ivan
 */

@ApplicationScoped
public class ImageService implements IImageService {

  @Override
  public Image add(ImageDTO dto) {

    Image image = new Image();
    image.userId = dto.getUserId();
    image.name = dto.getName();
    image.path = dto.getPath();
    image.tag = dto.getTag();

    image.persistAndFlush();

    return image;
  }

  @Override
  public boolean delete(UUID id) {
    Image image = Image.findById(id);

    if (image != null) {
      Host h = Host.find("image", image).firstResult();
      if (h != null) {
        h.image.delete();
      }
      image.delete();
      return true;
    }
    return false;
  }

  @Override
  public void deleteAll() {
    Image.deleteAll();
  }

  @Override
  public Image update(UUID id, ImageDTO dto) {

    Image image = Image.findById(id);

    if (image != null) {
      image.name = dto.getName();
      image.path = dto.getPath();
      image.tag = dto.getTag();

      image.persistAndFlush();
    }

    return image;
  }

  @Override
  public Image get(UUID id) {
    return Image.findById(id);
  }

  @Override
  public List<Image> list() {
    return Image.listAll();
  }

  @Override
  public List<Image> listPages(UriInfo info) {

    PanacheQuery<Image> listPage =
        Image
            .find("user_id",
                UUID.fromString(info.getQueryParameters().getFirst("user_id"))
            );

    Page page = new Page(
        Integer.parseInt(info.getQueryParameters().getFirst("index")),
        Integer.parseInt(info.getQueryParameters().getFirst("size"))
    );
    listPage.page(page);

    return listPage.list();
  }
}
