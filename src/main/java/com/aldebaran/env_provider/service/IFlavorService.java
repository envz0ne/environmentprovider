package com.aldebaran.env_provider.service;

import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.creation.FlavorCreateDTO;
import java.util.List;
import java.util.UUID;

public interface IFlavorService {

  FlavorDTO add(FlavorCreateDTO dto);

  boolean delete(UUID id);

  void deleteAll();

  FlavorDTO update(UUID id, FlavorCreateDTO dto);

  FlavorDTO get(UUID id);

  List<FlavorDTO> list();
}
