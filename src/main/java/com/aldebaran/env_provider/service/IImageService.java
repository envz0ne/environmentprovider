package com.aldebaran.env_provider.service;

import com.aldebaran.env_provider.dto.ImageDTO;
import com.aldebaran.env_provider.model.Image;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.core.UriInfo;

public interface IImageService {

  Image add(ImageDTO dto);

  boolean delete(UUID id);

  void deleteAll();

  Image update(UUID id, ImageDTO dto);

  Image get(UUID id);

  List<Image> list();

  List<Image> listPages(UriInfo info);
}
