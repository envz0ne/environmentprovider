package com.aldebaran.env_provider.service;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.ProviderConnectionDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.creation.ProviderCreateDTO;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.core.UriInfo;

public interface IProviderService {

  ProviderDTO add(ProviderCreateDTO dto);

  ProviderDTO addEnvironment(UUID id, EntityIdDTO env);

  boolean delete(UUID id);

  void deleteAll();

  boolean deleteEnvironments(UUID id);

  ProviderDTO update(UUID id, ProviderCreateDTO dto);

  ProviderDTO get(UUID id);

  List<EnvironmentDTO> getEnvironments(UUID id);

  List<ProviderDTO> list();

  List<ProviderConnectionDTO> listPages(UriInfo info);
}
