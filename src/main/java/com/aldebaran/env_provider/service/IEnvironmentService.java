package com.aldebaran.env_provider.service;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.EnvironmentInstanceDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.creation.EnvFullCreationDTO;
import com.aldebaran.env_provider.dto.creation.EnvironmentCreateDTO;
import com.aldebaran.env_provider.model.EnvironmentInstance;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.core.UriInfo;

public interface IEnvironmentService {

  EnvironmentDTO add(EnvironmentCreateDTO dto);

  EnvironmentDTO addFull(EnvFullCreationDTO dto);

  EnvironmentDTO addProvider(UUID id, EntityIdDTO providerId);

  EnvironmentDTO addHost(UUID id, EntityIdDTO hostId);

  EnvironmentDTO update(UUID id, EnvFullCreationDTO dto);

  EnvironmentDTO get(UUID id);

  boolean removeProvider(UUID id);

  boolean delete(UUID id);

  boolean deleteHosts(UUID id);

  void deleteAll();

  ProviderDTO getProvider(UUID id);

  List<EnvironmentDTO> list();

  List<EnvironmentDTO> listPages(UriInfo uriInfo);

  List<EnvironmentInstanceDTO> listEnvInstancePages(UriInfo uriInfo);

  List<HostDTO> hostList(UUID id);

  EnvironmentInstanceDTO getEnvInstance(UUID envInstanceId);

  List<EnvironmentInstanceDTO> listEnvInstances();

  List<EnvironmentInstanceDTO> listEnvInstancesForUser(UUID userId);
}

