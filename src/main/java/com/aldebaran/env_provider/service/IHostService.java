package com.aldebaran.env_provider.service;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.creation.HostCreateDTO;
import com.aldebaran.env_provider.model.Image;
import java.util.List;
import java.util.UUID;

public interface IHostService {

  HostDTO add(HostCreateDTO dto);

  HostDTO addFlavor(UUID id, EntityIdDTO flavorId);

  boolean delete(UUID id);

  void deleteAll();

  boolean deleteFlavorList(UUID id);

  HostDTO update(UUID id, HostCreateDTO dto);

  HostDTO get(UUID id);

  List<HostDTO> list();

  List<FlavorDTO> flavorList(UUID id);

  Image getImageById(UUID id);

  boolean deleteImage(UUID id);

  UUID getEnvironment(UUID id);

  HostDTO addEnvironment(UUID id, EntityIdDTO envId);

  boolean deleteEnvironment(UUID id);
}
