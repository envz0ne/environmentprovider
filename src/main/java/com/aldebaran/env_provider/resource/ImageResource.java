package com.aldebaran.env_provider.resource;

import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.ImageDTO;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.service.impl.ImageService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@Path("images")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
@RolesAllowed({"user","admin"})
public class ImageResource {

  @Inject
  ImageService service;

  @GET
  public Response list() {

    List<Image> list = service.list();

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @GET
  @Path("/pages")
  @PermitAll
  public Response listByPage(@Context UriInfo info) {

    List<Image> list = service.listPages(info);

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @POST
  @Transactional
  public Response add(ImageDTO dto) {
    return Response.ok(service.add(dto)).build();
  }

  @DELETE
  @Transactional
  public void deleteAll() {
    service.deleteAll();
  }

  @Path("/{id}")
  @GET
  public Response getImage(@PathParam("id") UUID id) {

    Image image = service.get(id);

    if (image == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid image id: " + id).build();
    } else {
      return Response.ok(image).build();
    }
  }

  @Path("/{id}")
  @PUT
  @Transactional
  public Response update(@PathParam("id") UUID id, ImageDTO dto) {

    Image image = service.update(id, dto);

    if (image == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid image id: " + id).build();
    } else {
      return Response.ok(image).build();
    }
  }

  @Path("/{id}")
  @DELETE
  @Transactional
  public Response delete(@PathParam("id") UUID id) {

    if (service.delete(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid image id: " + id).build();
    }
  }
}