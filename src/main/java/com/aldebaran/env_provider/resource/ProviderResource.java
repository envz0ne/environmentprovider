package com.aldebaran.env_provider.resource;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.ProviderConnectionDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.creation.ProviderCreateDTO;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.model.Provider;
import com.aldebaran.env_provider.service.impl.ProviderService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@Path("providers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
@RolesAllowed({"user","admin"})
public class ProviderResource {

  @Inject
  ProviderService service;

  @GET
  public Response list() {

    List<ProviderDTO> list = service.list();

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @GET
  @Path("/pages")
  @PermitAll
  public Response listByPage(@Context UriInfo info) {

    List<ProviderConnectionDTO> list = service.listPages(info);

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @POST
  @Transactional
  public Response add(ProviderCreateDTO dto) {

    if (service.add(dto) == null) {
      return Response
          .status(Status.BAD_REQUEST)
          .entity("Provider with " + dto.getName() + " name is already exsits.")
          .build();
    }

    return Response.ok(service.add(dto)).build();
  }

  @DELETE
  @Transactional
  public void deleteAll() {
    service.deleteAll();
  }

  @Path("/{id}")
  @GET
  public Response get(@PathParam("id") UUID id) {

    ProviderDTO provider = service.get(id);

    if (provider == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + id).build();
    } else {
      return Response.ok(provider).build();
    }
  }

  @Path("/{id}")
  @PUT
  @Transactional
  public Response update(@PathParam("id") UUID id, ProviderCreateDTO dto) {

    ProviderDTO provider = service.update(id, dto);

    if (provider == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + id).build();
    } else {
      return Response.ok(provider).build();
    }
  }

  @Path("/{id}")
  @DELETE
  @Transactional
  public Response delete(@PathParam("id") UUID id) {

    if (service.delete(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + id).build();
    }
  }

  @Path("/{id}/environments")
  @GET
  public Response getEnvironments(@PathParam("id") UUID id) {

    List<EnvironmentDTO> envs = service.getEnvironments(id);

    if (envs == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + id).build();
    } else {
      return Response.ok(envs).build();
    }
  }

  @Path("/{id}/environments")
  @POST
  @Transactional
  public Response addEnvironment(@PathParam("id") UUID id, EntityIdDTO entityId) {

    ProviderDTO provider = service.addEnvironment(id, entityId);

    if (provider == null) {

      if (Provider.findById(id) == null) {
        return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + id).build();
      } else {
        return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + entityId).build();
      }
    } else {
      return Response.ok(provider).build();
    }
  }

  @Path("/{id}/environments")
  @DELETE
  @Transactional
  public void deleteEnvironments(@PathParam("id") UUID id) {
    service.deleteEnvironments(id);
  }
}