package com.aldebaran.env_provider.resource;

import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.creation.HostCreateDTO;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.Image;
import com.aldebaran.env_provider.service.impl.HostService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("hosts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
@RolesAllowed({"user","admin"})
public class HostResource {

  @Inject
  HostService service;

  @GET
  public Response list() {
    List<HostDTO> list = service.list();

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @POST
  @Transactional
  public Response add(HostCreateDTO dto) {
    return Response.ok(service.add(dto)).build();
  }

  @DELETE
  @Transactional
  public void deleteAll() {
    service.deleteAll();
  }

  @Path("/{id}")
  @GET
  public Response getById(@PathParam("id") UUID id) {

    HostDTO host = service.get(id);

    if (host == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    } else {
      return Response.ok(host).build();
    }
  }

  @Path("/{id}")
  @PUT
  @Transactional
  public Response update(@PathParam("id") UUID id, HostCreateDTO hostModified) {

    HostDTO host = service.update(id, hostModified);

    if (host == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    } else {
      return Response.ok(host).build();
    }
  }

  @Path("/{id}")
  @DELETE
  @Transactional
  public Response deleteHostById(@PathParam("id") UUID id) {

    if (service.delete(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    }
  }

  @Path("/{id}/flavors")
  @GET
  public Response hostFlavorList(@PathParam("id") UUID id) {

    List<FlavorDTO> flavors = service.flavorList(id);

    if (flavors == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    } else {
      return Response.ok(flavors).build();
    }
  }

  @Path("/{id}/flavors")
  @POST
  @Transactional
  public Response addFlavorToHost(@PathParam("id") UUID id, EntityIdDTO flavorId) {

    HostDTO host = service.addFlavor(id, flavorId);

    if (host == null) {

      if (Host.findById(id) == null) {
        return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
      } else {
        return Response.status(Status.NOT_FOUND).entity("Invalid flavor id: " + flavorId).build();
      }
    } else {
      return Response.ok(host).build();
    }
  }

  @Path("/{id}/flavors")
  @DELETE
  @Transactional
  public Response removeFlavorListFromHost(@PathParam("id") UUID id) {

    if (service.deleteFlavorList(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    }
  }

  @Path("/{id}/images")
  @GET
  public Response getHostImage(@PathParam("id") UUID id) {

    Image image = service.getImageById(id);

    if (image == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    } else {
      return Response.ok(image).build();
    }
  }

  @Path("/{id}/images")
  @DELETE
  public Response deleteHostImage(@PathParam("id") UUID id) {

    if (service.deleteImage(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    }
  }

  @Path("/{id}/environments")
  @GET
  public Response getEnvironment(@PathParam("id") UUID id) {

    UUID envId = service.getEnvironment(id);

    if (envId == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    } else {
      return Response.ok(envId).build();
    }
  }

  @Path("/{id}/environments")
  @POST
  @Transactional
  public Response addEnvironment(@PathParam("id") UUID id, EntityIdDTO envId) {

    HostDTO host = service.addEnvironment(id, envId);

    if (host == null) {
      if (Host.findById(id) == null) {
        return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
      } else {
        return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + envId).build();
      }
    } else {
      return Response.ok(host).build();
    }
  }

  @Path("/{id}/environments")
  @DELETE
  @Transactional
  public Response deleteEnvironmentsFromHost(@PathParam("id") UUID id) {

    if (service.deleteEnvironment(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
    }
  }
}