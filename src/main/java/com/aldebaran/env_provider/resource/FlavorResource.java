package com.aldebaran.env_provider.resource;

import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.dto.creation.FlavorCreateDTO;
import com.aldebaran.env_provider.service.impl.FlavorService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("flavors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
@RolesAllowed({"user","admin"})
public class FlavorResource {

  @Inject
  FlavorService service;

  @GET
  public Response list() {

    List<FlavorDTO> list = service.list();

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @POST
  @Transactional
  public Response add(FlavorCreateDTO dto) {
    return Response.ok(service.add(dto)).build();
  }

  @DELETE
  @Transactional
  public void deleteAll() {
    service.deleteAll();
  }

  @Path("/{id}")
  @GET
  public Response get(@PathParam("id") UUID id) {

    FlavorDTO flavor = service.get(id);

    if (flavor == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid flavor id: " + id).build();
    } else {
      return Response.ok(flavor).build();
    }
  }

  @Path("/{id}")
  @PUT
  @Transactional
  public Response update(@PathParam("id") UUID id, FlavorCreateDTO dto) {

    FlavorDTO flavor = service.get(id);

    if (flavor == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid flavor id: " + id).build();
    } else {
      return Response.ok(flavor).build();
    }
  }

  @Path("/{id}")
  @DELETE
  @Transactional
  public Response delete(@PathParam("id") UUID id) {

    if (service.delete(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid flavor id: " + id).build();
    }
  }
}