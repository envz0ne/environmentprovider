package com.aldebaran.env_provider.resource;

import com.aldebaran.env_provider.adapter.IProviderAdapter;
import com.aldebaran.env_provider.adapter.utils.EnvironmentStatus;
import com.aldebaran.env_provider.dto.EntityIdDTO;
import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.EnvironmentInstanceDTO;
import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.dto.adapter.environment.EnvironmentResponseDTO;
import com.aldebaran.env_provider.dto.adapter.environment.EnvironmentStatusDTO;
import com.aldebaran.env_provider.dto.adapter.environment.HostsStatusDTO;
import com.aldebaran.env_provider.dto.creation.EnvFullCreationDTO;
import com.aldebaran.env_provider.dto.creation.EnvironmentCreateDTO;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.EnvironmentInstance;
import com.aldebaran.env_provider.service.impl.EnvironmentService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@Path("environments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class EnvironmentResource {

  @Inject
  EnvironmentService service;

  @Inject
  IProviderAdapter dockerAdapter;

  @GET
  @PermitAll
  public Response list() {

    List<EnvironmentDTO> list = service.list();

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @GET
  @Path("/pages")
  @PermitAll
  public Response listByPage(@Context UriInfo info) {

    List<EnvironmentDTO> list = service.listPages(info);

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @GET
  @Path("/instances/pages")
  @PermitAll
  public Response listEnvInstanceByPage(@Context UriInfo info) {

    List<EnvironmentInstanceDTO> list = service.listEnvInstancePages(info);

    if (list.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(list).build();
    }
  }

  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response add(EnvironmentCreateDTO dto) {

    if (service.add(dto) == null) {

      return Response
          .status(Status.BAD_REQUEST)
          .entity("Environment with " + dto.getName() + " name is already exsits.")
          .build();
    }

    return Response.ok(service.add(dto)).build();
  }

  @Path("/{id}")
  @GET
  @RolesAllowed({"user", "admin"})
  public Response getEnvironmentById(@PathParam("id") UUID id) {

    EnvironmentDTO env = service.get(id);

    if (env == null) {
      return Response.status(Status.NOT_FOUND).build();
    } else {
      return Response.ok(env).build();
    }
  }

  @Path("/full-creation")
  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response addFull(EnvFullCreationDTO dto) {

    EnvironmentDTO res = service.addFull(dto);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (res == null) {
      statusDTO.setStatus(EnvironmentStatus.ALREDY_CREATED);
      statusDTO.setMsg("Environment with " + dto.getName() + " name is already exsits.");

      responseDTO.setInfo(statusDTO);

      return Response
          .status(Status.BAD_REQUEST)
          .entity(responseDTO)
          .build();
    }

    statusDTO.setStatus(EnvironmentStatus.CREATED);
    statusDTO.setMsg("Environment with name: " + dto.getName() + " was created.");

    responseDTO.setInfo(statusDTO);
    responseDTO.setId(res.getId());

    return Response.ok(responseDTO).build();
  }

  @Path("/{id}/update")
  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response updateEnvironment(@PathParam("id") UUID id, EnvFullCreationDTO dto) {

    EnvironmentDTO env = service.update(id, dto);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (env == null) {
      statusDTO.setStatus(EnvironmentStatus.UNKONWN);
      statusDTO.setMsg("Environment with id: " + id + " doesn't exist");

      responseDTO.setInfo(statusDTO);
      responseDTO.setId(id);

      return Response.status(Status.NOT_FOUND).entity(responseDTO).build();
    } else {

      statusDTO.setStatus(EnvironmentStatus.UPDATED);
      statusDTO.setMsg("Environment with  name: " + dto.getName() + " was updated.");

      responseDTO.setInfo(statusDTO);
      responseDTO.setId(env.getId());

      return Response.ok(responseDTO).build();
    }
  }

  @DELETE
  @Transactional
  @RolesAllowed({"user", "admin"})
  public void deleteAll() {
    service.deleteAll();
  }

  @Path("/{id}")
  @DELETE
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response deleteEnvironmentById(@PathParam("id") UUID id) {

    if (service.delete(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
    }
  }

  // --------------------------------------------- Environment's properties --------------------------------------------

  @Path("/{id}/hosts")
  @GET
  @RolesAllowed({"user", "admin"})
  public Response environmentHostList(@PathParam("id") UUID id) {

    List<HostDTO> hosts = service.hostList(id);

    if (hosts == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
    } else {
      return Response.ok(hosts).build();
    }
  }

  @Path("/{id}/hosts")
  @POST
  @Transactional
  @RolesAllowed({"admin"})
  public Response addHost(@PathParam("id") UUID id, EntityIdDTO entityId) {

    EnvironmentDTO env = service.addHost(id, entityId);

    if (env == null) {
      if (Environment.findById(id) == null) {
        return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
      } else {
        return Response.status(Status.NOT_FOUND).entity("Invalid host id: " + id).build();
      }
    } else {
      return Response.ok(env).build();
    }
  }

  @Path("/{id}/hosts")
  @DELETE
  @Transactional
  @RolesAllowed({"admin"})
  public Response deleteHostList(@PathParam("id") UUID id) {
    if (service.deleteHosts(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
    }
  }

  @Path("/{id}/providers")
  @GET
  @RolesAllowed({"user", "admin"})
  public Response getProvider(@PathParam("id") UUID id) {

    ProviderDTO provider = service.getProvider(id);

    if (provider == null) {
      return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
    } else {
      return Response.ok(provider).build();
    }
  }

  @Path("/{id}/providers")
  @POST
  @Transactional
  @RolesAllowed({"admin"})
  public Response addProvider(@PathParam("id") UUID id, EntityIdDTO entityId) {

    EnvironmentDTO env = service.addProvider(id, entityId);

    if (env == null) {

      if (Environment.findById(id) == null) {
        return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
      } else {
        return Response.status(Status.NOT_FOUND).entity("Invalid provider id: " + entityId.getId()).build();
      }
    } else {
      return Response.ok(env).build();
    }
  }

  @Path("/{id}/providers")
  @DELETE
  @Transactional
  @RolesAllowed({"admin"})
  public Response removeProvider(@PathParam("id") UUID id) {

    if (service.removeProvider(id)) {
      return Response.ok().build();
    } else {
      return Response.status(Status.NOT_FOUND).entity("Invalid environment id: " + id).build();
    }
  }

  // ------------------------------------- Environment instance methods ------------------------------------------------

  @Path("/{name}/pull")
  @POST
  @RolesAllowed({"user", "admin"})
  public Response setUpEnvironment(@PathParam("name") String name) {

    EnvironmentResponseDTO response = new EnvironmentResponseDTO();
    Environment environment = Environment.find("name", name).firstResult();

    if (environment == null) {
      EnvironmentStatusDTO status = new EnvironmentStatusDTO();

      response.setId(null);
      status.setMsg("Environment doesn't exist");
      status.setStatus(EnvironmentStatus.UNKONWN);
      response.setInfo(status);

      return Response.status(Status.NOT_FOUND).entity(response).build();
    }

    switch (environment.provider.type) {
      case "docker":
        dockerAdapter
            .initClient(environment.provider.name, environment.provider.url);

        response = dockerAdapter.pullImage(environment);
        break;

      case "k8s":
        break;
    }

    return Response.ok(response).build();
  }


  /**
   * Run the new environment instance.
   *
   * @param name of the environment config that should be up.
   * @return environment up
   */
  @Path("/{name}/initiate")
  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response runEnvironment(@PathParam("name") String name) {

    EnvironmentResponseDTO response = new EnvironmentResponseDTO();
    Environment environment = Environment.find("name", name).firstResult();

    if (environment == null) {
      EnvironmentStatusDTO status = new EnvironmentStatusDTO();

      response.setId(null);
      status.setMsg("Environment doesn't exist");
      status.setStatus(EnvironmentStatus.UNKONWN);
      response.setInfo(status);

      return Response.status(Status.NOT_FOUND).entity(response).build();
    }

    switch (environment.provider.type) {
      case "docker":
        response = dockerAdapter.run(environment);
        break;

      case "k8s":
        break;
    }

    return Response.ok(response).build();
  }

  /**
   * Terminate running environment instance.
   *
   * @param id of the specific environment instance
   * @return environment instance response.
   */
  @Path("/{id}/terminate")
  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response stopEnvironmentInstance(@PathParam("id") UUID id) {

    EnvironmentResponseDTO response = new EnvironmentResponseDTO();
    EnvironmentInstance envInstance = EnvironmentInstance.findById(id);

    if (envInstance == null) {

      EnvironmentStatusDTO status = new EnvironmentStatusDTO();
      response.setId(null);
      status.setMsg("Environment instance doesn't exist");
      status.setStatus(EnvironmentStatus.UNKONWN);
      response.setInfo(status);

      return Response.status(Status.NOT_FOUND).entity(response).build();
    }

    switch (envInstance.environment.provider.type) {
      case "docker":
        response = dockerAdapter
            .stopAndRemove(envInstance);

        /*if (EnvironmentInstance
            .find("environment.provider.name", envInstance.environment.provider.name)
            .count() == 0) {
            dockerAdapter.deleteClient(envInstance.environment.provider.name);
          System.out.println("Was deleted");
        }*/
        break;
      case "k8s":
        break;
    }

    return Response.ok(response).build();
  }

  /**
   * Restart environment instance
   *
   * @param id of the specific environment instance
   * @return evironment running instance status
   */
  @Path("/{id}/restart")
  @POST
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response restartEnvironment(@PathParam("id") UUID id) {

    EnvironmentResponseDTO response = new EnvironmentResponseDTO();
    EnvironmentInstance envInstance = EnvironmentInstance.findById(id);

    if (envInstance == null) {

      EnvironmentStatusDTO status = new EnvironmentStatusDTO();
      response.setId(null);
      status.setMsg("Environment instance doesn't exist");
      status.setStatus(EnvironmentStatus.UNKONWN);
      response.setInfo(status);

      return Response.status(Status.NOT_FOUND).entity(response).build();
    }

    response.setId(envInstance.id);

    switch (envInstance.environment.provider.type) {
      case "docker":
        response = dockerAdapter
            .restart(envInstance);
        break;
      case "k8s":
        break;
    }

    return Response.ok(response).build();
  }

  /**
   * Get environment instance hosts' status
   *
   * @param id of the specific environment instance
   * @return environment instance model
   */
  @Path("/{id}/hosts/status")
  @GET
  @Transactional
  @RolesAllowed({"user", "admin"})
  public Response getEnvironmentHostsStatus(@PathParam("id") UUID id) {

    HostsStatusDTO response = new HostsStatusDTO();
    EnvironmentStatusDTO envStatus = new EnvironmentStatusDTO();

    EnvironmentInstance envInstance = EnvironmentInstance.findById(id);

    // check existence
    if (envInstance == null) {

      response.setEnvId(id);
      response.setHosts(null);

      envStatus.setMsg("No such environment instance with id: " + id);
      envStatus.setStatus(EnvironmentStatus.UNKONWN);

      response.setEnvStatus(envStatus);

      return Response.status(Status.NOT_FOUND).entity(response).build();
    }

    response.setEnvId(envInstance.id);

    switch (envInstance.environment.provider.type) {
      case "docker":
        response = dockerAdapter
            .getEnvHostsStatus(envInstance);

        envStatus.setStatus(EnvironmentStatus.RUNNING);
        break;
      case "k8s":
        break;
    }

    return Response.ok(response).build();
  }

  /**
   * Get environment instance model: env instance id, env id, env's provider and env instance's hosts
   *
   * @param id of the specific environment instance
   * @return environment instance model
   */
  @Path("/instances/{id}")
  @GET
  @RolesAllowed({"user", "admin"})
  public Response getEnvironmentInstance(@PathParam("id") UUID id) {

    EnvironmentInstanceDTO env = service.getEnvInstance(id);

    if (env == null) {
      return Response.status(Status.NOT_FOUND)
          .entity("Environment instance with id: " + id + " cannot be sent").build();
    } else {
      return Response.ok(env).build();
    }
  }

  /**
   * Get environment instance model: env instance id, env id, env's provider and env instance's hosts
   *
   * @return environment instances
   */
  @Path("/instances")
  @GET
  @PermitAll
  public Response listEnvironmentInstances() {

    List<EnvironmentInstanceDTO> instances = service.listEnvInstances();

    if (instances == null) {
      return Response.status(Status.NOT_FOUND)
          .entity("No env's instances").build();
    } else {
      return Response.ok(instances).build();
    }
  }

}