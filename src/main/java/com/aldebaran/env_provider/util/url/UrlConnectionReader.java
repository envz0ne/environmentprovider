package com.aldebaran.env_provider.util.url;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class UrlConnectionReader {

  private static String getUrlContents(URL url) {
    StringBuilder content = new StringBuilder();

    try {
      // create a urlConnection object
      URLConnection urlConnection = url.openConnection();

      // wrap the urlConnection in a bufferedReader
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

      String line;

      // read from the urlConnection via the bufferedReader
      while ((line = bufferedReader.readLine()) != null) {
        content.append(line).append("\n");
      }
      bufferedReader.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return content.toString();
  }
}