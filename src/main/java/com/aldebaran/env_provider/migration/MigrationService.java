package com.aldebaran.env_provider.migration;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.flywaydb.core.Flyway;

/**
 * Represents the service for applying Flyaway's migration methods.
 *
 * @author Tarasov Ivan
 */

@ApplicationScoped
public class MigrationService {

  @Inject
  Flyway flyway;

  /**
   * Print current version of the migration state.
   */

  public void checkMigration() {
    System.out.println(flyway.info().current().getVersion().toString());
  }

}