package com.aldebaran.env_provider.dto;

import java.util.UUID;

public class EntityIdDTO {

  private UUID id;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }
}