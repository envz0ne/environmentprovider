package com.aldebaran.env_provider.dto;

import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProviderDTO {

  private UUID id;

  @NotBlank
  private String name;

  @NotNull
  private String url;

  @NotBlank
  private String type;

  private List<EnvironmentDTO> environments;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<EnvironmentDTO> getEnvironments() {
    return environments;
  }

  public void setEnvironments(List<EnvironmentDTO> environments) {
    this.environments = environments;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
