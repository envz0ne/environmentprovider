package com.aldebaran.env_provider.dto.adapter.environment;

import javax.validation.constraints.NotBlank;

public class EnvironmentRunDTO {

  @NotBlank
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
