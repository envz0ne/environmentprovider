package com.aldebaran.env_provider.dto.adapter.environment;

import com.aldebaran.env_provider.dto.adapter.host.HostStatusDTO;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;

public class HostsStatusDTO {

  @NotNull
  private UUID envId;

  @NotNull
  private List<HostStatusDTO> hosts;

  @NotNull
  private EnvironmentStatusDTO envStatus;

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public List<HostStatusDTO> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostStatusDTO> hosts) {
    this.hosts = hosts;
  }

  public EnvironmentStatusDTO getEnvStatus() {
    return envStatus;
  }

  public void setEnvStatus(EnvironmentStatusDTO envStatus) {
    this.envStatus = envStatus;
  }
}
