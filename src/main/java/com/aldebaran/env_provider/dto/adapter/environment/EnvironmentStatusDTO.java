package com.aldebaran.env_provider.dto.adapter.environment;

import com.aldebaran.env_provider.adapter.utils.EnvironmentStatus;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EnvironmentStatusDTO {

  @NotNull
  private EnvironmentStatus status;

  @NotBlank
  private String msg;

  public EnvironmentStatus getStatus() {
    return status;
  }

  public void setStatus(EnvironmentStatus status) {
    this.status = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}
