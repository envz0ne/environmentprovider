package com.aldebaran.env_provider.dto.adapter.host;

import com.aldebaran.env_provider.adapter.utils.HostStatus;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class HostStatusDTO {

  @NotNull
  private String id;

  @NotNull
  private HostStatus status;

  @NotBlank
  private String msg;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public HostStatus getStatus() {
    return status;
  }

  public void setStatus(HostStatus status) {
    this.status = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}
