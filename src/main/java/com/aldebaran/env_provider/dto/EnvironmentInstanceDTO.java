package com.aldebaran.env_provider.dto;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;

public class EnvironmentInstanceDTO {

  /**
   * Environment instance id
   */
  @NotNull
  private UUID id;

  /**
   * Environment id.
   */
  @NotNull
  private UUID envId;

  @NotNull
  private String envName;

  @NotNull
  private ZonedDateTime createdAt;

  @NotNull
  private ProviderConnectionDTO provider;

  private List<HostInstanceDTO> hosts;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public ProviderConnectionDTO getProvider() {
    return provider;
  }

  public void setProvider(ProviderConnectionDTO provider) {
    this.provider = provider;
  }

  public List<HostInstanceDTO> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostInstanceDTO> hosts) {
    this.hosts = hosts;
  }

  public String getEnvName() {
    return envName;
  }

  public void setEnvName(String envName) {
    this.envName = envName;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }
}