package com.aldebaran.env_provider.dto;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public class FlavorDTO {

  private UUID id;

  @NotBlank
  private String name;

  @NotBlank
  private String mountPoint;

  @Positive
  private int size;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMountPoint() {
    return mountPoint;
  }

  public void setMountPoint(String mountPoint) {
    this.mountPoint = mountPoint;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }
}
