package com.aldebaran.env_provider.dto;

import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;

public class EnvironmentDTO {

  private UUID id;

  @NotBlank
  private String name;

  private List<HostDTO> hosts;

  private ProviderConnectionDTO provider;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<HostDTO> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostDTO> hosts) {
    this.hosts = hosts;
  }

  public ProviderConnectionDTO getProvider() {
    return provider;
  }

  public void setProvider(ProviderConnectionDTO provider) {
    this.provider = provider;
  }
}
