package com.aldebaran.env_provider.dto.creation;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EnvironmentCreateDTO {

  @NotBlank
  private String name;

  @NotNull
  private UUID providerId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UUID getProviderId() {
    return providerId;
  }

  public void setProviderId(UUID providerId) {
    this.providerId = providerId;
  }
}
