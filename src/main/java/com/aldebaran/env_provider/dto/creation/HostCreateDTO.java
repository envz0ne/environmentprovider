package com.aldebaran.env_provider.dto.creation;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class HostCreateDTO {

  @NotBlank
  private String name;

  @NotNull
  private String url;

  @Positive
  private int ram;

  @Positive
  private int vcpu;

  @NotNull
  private UUID envId;

  @NotNull
  private UUID imageId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getRam() {
    return ram;
  }

  public void setRam(int ram) {
    this.ram = ram;
  }

  public int getVcpu() {
    return vcpu;
  }

  public void setVcpu(int vcpu) {
    this.vcpu = vcpu;
  }

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public UUID getImageId() {
    return imageId;
  }

  public void setImageId(UUID imageId) {
    this.imageId = imageId;
  }
}
