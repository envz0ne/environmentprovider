package com.aldebaran.env_provider.dto;

import com.aldebaran.env_provider.model.Image;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class HostDTO {

  private UUID id;

  private UUID envId;

  @NotBlank
  private String name;

  @NotNull
  private String url;

  @Positive
  private int ram;

  @Positive
  private int vcpu;

  private List<FlavorDTO> flavors;

  private Image image;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getRam() {
    return ram;
  }

  public void setRam(int ram) {
    this.ram = ram;
  }

  public int getVcpu() {
    return vcpu;
  }

  public void setVcpu(int vcpu) {
    this.vcpu = vcpu;
  }

  public List<FlavorDTO> getFlavors() {
    return flavors;
  }

  public void setFlavors(List<FlavorDTO> flavors) {
    this.flavors = flavors;
  }

  public Image getImage() {
    return image;
  }

  public void setImage(Image image) {
    this.image = image;
  }
}
