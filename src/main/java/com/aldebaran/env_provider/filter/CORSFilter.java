package com.aldebaran.env_provider.filter;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class CORSFilter implements ContainerResponseFilter {

  @Override
  public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext responseContext)
      throws IOException {
    responseContext.getHeaders().add(
        "Access-Control-Allow-Origin", "*");
    responseContext.getHeaders().add(
        "Access-Control-Allow-Credentials", "true");
    responseContext.getHeaders().add(
        "Access-Control-Allow-Headers",
        "origin, content-type, accept, authorization, Cache-Control, Pragma, Expires, Access-Control-Allow-Origin");
    responseContext.getHeaders().add(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, OPTIONS, HEAD");
  }
}