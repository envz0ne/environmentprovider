package com.aldebaran.env_provider.mapper;

import com.aldebaran.env_provider.dto.FlavorDTO;
import com.aldebaran.env_provider.model.Flavor;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = MappingConfig.class)
public interface FlavorMapper {

  FlavorDTO toDTO(Flavor flavor);

  List<FlavorDTO> toDTOList(List<Flavor> list);

  Flavor toEntity(FlavorDTO dto);
}
