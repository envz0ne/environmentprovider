package com.aldebaran.env_provider.mapper;

import com.aldebaran.env_provider.dto.EnvironmentDTO;
import com.aldebaran.env_provider.dto.EnvironmentInstanceDTO;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.EnvironmentInstance;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(config = MappingConfig.class,
    uses = {
        ProviderMapper.class,
        HostMapper.class
    })
public interface EnvironmentMapper {

  EnvironmentDTO toDTO(Environment environment);

  List<EnvironmentDTO> toDTOList(List<Environment> list);

  List<Environment> toList(List<EnvironmentDTO> dtoList);

  Environment fromDTO(EnvironmentDTO env);

  @Mappings({
      @Mapping(target = "provider", source = "environment.provider"),
      @Mapping(target = "envId", source = "environment.id"),
      @Mapping(target = "envName", source = "environment.name"),
      @Mapping(target = "hosts", source = "hostInstances"),
  })
  EnvironmentInstanceDTO toEnvInstanceDTO(EnvironmentInstance environmentInstance);

  List<EnvironmentInstanceDTO> toEnvInstanceDTOList(List<EnvironmentInstance> list);
}