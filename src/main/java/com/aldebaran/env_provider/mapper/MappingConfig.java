package com.aldebaran.env_provider.mapper;

import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "cdi")
public interface MappingConfig {

}
