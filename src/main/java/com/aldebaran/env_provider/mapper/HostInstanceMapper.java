package com.aldebaran.env_provider.mapper;

import com.aldebaran.env_provider.dto.HostInstanceDTO;
import com.aldebaran.env_provider.model.HostInstance;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = MappingConfig.class)
public interface HostInstanceMapper {

  HostInstanceDTO toDTO(HostInstance envInstance);

  List<HostInstanceDTO> toDTOList(List<HostInstance> list);

}
