package com.aldebaran.env_provider.mapper;

import com.aldebaran.env_provider.dto.ProviderConnectionDTO;
import com.aldebaran.env_provider.dto.ProviderDTO;
import com.aldebaran.env_provider.model.Provider;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = MappingConfig.class)
public interface ProviderMapper {

  ProviderDTO toDTO(Provider provider);

  List<ProviderDTO> toDTOList(List<Provider> providerList);

  ProviderConnectionDTO toConectionDTO(Provider provider);

  List<ProviderConnectionDTO> toConnectionDTOList(List<Provider> providerList);

  Provider toEntity(ProviderDTO add);
}
