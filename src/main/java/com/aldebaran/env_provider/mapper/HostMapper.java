package com.aldebaran.env_provider.mapper;

import com.aldebaran.env_provider.dto.HostDTO;
import com.aldebaran.env_provider.dto.HostInstanceDTO;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.HostInstance;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MappingConfig.class,
    uses = {
        EnvironmentMapper.class
    })
public interface HostMapper {

  @Mapping(target = "envId", source = "environment.id")
  HostDTO toDTO(Host host);

  List<HostDTO> toDTOList(List<Host> list);

  HostInstanceDTO toInstanceDTO(HostInstance instance);

  List<HostInstanceDTO> toInstanceDTOList(List<HostInstance> list);

  @Mapping(target = "environment.id", source = "envId")
  Host toEntity(HostDTO dto);

  List<Host> toEntityList(List<HostDTO> list);

}
