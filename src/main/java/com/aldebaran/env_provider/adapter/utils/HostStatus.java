package com.aldebaran.env_provider.adapter.utils;

public enum HostStatus {
  RUNNING,
  NOT_STARTED,
  REMOVED,
  NOT_REMOVED,
  PAUSED,
  NOT_PAUSED,
  UNPAUSED,
  NOT_UNPAUSED,
  STOPPED,
  NOT_STOPPED,
  EXITED,
  NOT_EXIST
}
