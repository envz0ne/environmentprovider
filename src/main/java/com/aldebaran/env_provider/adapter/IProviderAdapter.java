package com.aldebaran.env_provider.adapter;

import com.aldebaran.env_provider.dto.adapter.environment.EnvironmentResponseDTO;
import com.aldebaran.env_provider.dto.adapter.environment.HostsStatusDTO;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.EnvironmentInstance;

/**
 * Common base methods for adapters that represent providers of environments.
 *
 * @author Tarasov Ivan
 * @version 1.1
 */
public interface IProviderAdapter {

  /**
   * Run a new environment.
   *
   * @param env represents an environment
   * @return the object with an environment status after trying to run a new environment
   */
  EnvironmentResponseDTO run(Environment env);

  /**
   * Stop and remove a specific environment by id.
   *
   * @param env represents an environment instance
   * @return the object with an environment status after trying to stop the environment
   */
  EnvironmentResponseDTO stopAndRemove(EnvironmentInstance env);

  /**
   * Restart a specific environment by id.
   *
   * @param env represents an environment instance
   * @return the object with an environment status after trying to stop the environment
   */
  EnvironmentResponseDTO restart(EnvironmentInstance env);


  /**
   * Pause a specific environment by id.
   *
   * @param env represents an environment instance
   * @return the object with an environment status after trying to pause the environment
   */
  EnvironmentResponseDTO pause(EnvironmentInstance env);

  /**
   * Unpause a specific environment by id. Call this method after pausing this environment.
   *
   * @param env represents an environment instance
   * @return the object with an environment status after trying to unpause the environment
   */
  EnvironmentResponseDTO unpause(EnvironmentInstance env);

  /**
   * Pulling all images.
   *
   * @param environment for which pulling images
   */
  EnvironmentResponseDTO pullImage(Environment environment);

  /**
   * Get provider client's current info.
   *
   * @param providerName represents the specific provider that has its own client
   * @return the object that represent client current information
   */
  Object getClientInfo(String providerName);

  /**
   * @param env represents an environment
   * @return the object with an environment status
   */
  HostsStatusDTO getEnvHostsStatus(EnvironmentInstance env);

  /**
   * Initialize a new provider client.
   *
   * @param providerName represents the provider's name that map the client
   * @param providerUrl represents an url of the provider
   */
  void initClient(String providerName, String providerUrl);

  void deleteClient(String providerName);
}