package com.aldebaran.env_provider.adapter.impl;

import com.aldebaran.env_provider.adapter.IProviderAdapter;
import com.aldebaran.env_provider.adapter.utils.EnvironmentStatus;
import com.aldebaran.env_provider.adapter.utils.HostStatus;
import com.aldebaran.env_provider.dto.adapter.environment.EnvironmentResponseDTO;
import com.aldebaran.env_provider.dto.adapter.environment.EnvironmentStatusDTO;
import com.aldebaran.env_provider.dto.adapter.environment.HostsStatusDTO;
import com.aldebaran.env_provider.dto.adapter.host.HostStatusDTO;
import com.aldebaran.env_provider.model.Environment;
import com.aldebaran.env_provider.model.EnvironmentInstance;
import com.aldebaran.env_provider.model.Flavor;
import com.aldebaran.env_provider.model.Host;
import com.aldebaran.env_provider.model.HostInstance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.DockerCmdExecFactory;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ContainerNetwork;
import com.github.dockerjava.api.model.ContainerNetworkSettings;
import com.github.dockerjava.api.model.ContainerPort;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.PullResponseItem;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.PullImageResultCallback;
import com.github.dockerjava.jaxrs.JerseyDockerCmdExecFactory;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

/**
 * Represents an adapter for managing the certain environment by the docker daemon.
 *
 * See <a href="https://github.com/docker-java/docker-java">http://google.com</a>
 *
 * @author Tarasov Ivan
 */
@ApplicationScoped
public class DockerAdapterService implements IProviderAdapter {

  private DockerClient dockerClient;

  @ConfigProperty(name = "adapter.provider.first-bind-port")
  int firstBindPort;

  @ConfigProperty(name = "adapter.docker.read-timeout")
  int clientReadTimeout;

  @ConfigProperty(name = "adapter.docker.connect-timeout")
  int clientConnectTimeout;

  @ConfigProperty(name = "adapter.docker.max-total-connections")
  int clientMaxTotalConnections;

  @ConfigProperty(name = "adapter.docker.max-per-route-connections")
  int clientMaxPerRouteConnections;

  @ConfigProperty(name = "adapter.docker.deamon.port")
  private int dockerDeamonPort;

  @ConfigProperty(name = "service.time.zone")
  private String zone;

  private static final Logger LOGGER = Logger.getLogger(DockerAdapterService.class);

  /**
   * Represents a map of the docker clients for the specific provider.
   *
   * Key: name of the provider Value: docker client
   */
  private Map<String, DockerClient> clients = new HashMap<>();

  public DockerAdapterService() {
  }

  /**
   * Run the specific environment.
   *
   * @param environment that needs to run
   * @return response that shows how running was
   */
  @Override
  public EnvironmentResponseDTO run(Environment environment) {

    EnvironmentResponseDTO result = null;
    boolean isExisted = false;

    // Get dockerClient
    dockerClient = clients.get(environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    // Create a new environment instance
    EnvironmentInstance envInstance = new EnvironmentInstance();

    envInstance.environment = environment;

    envInstance.persist();

    responseDTO.setId(envInstance.id);

    try {

      //By the environment configuration create new containers

      // Host = container
      for (Host host : environment.hosts) {

        /*

        // Check if we already have a container with hosts' name
        List<Container> dockerSearch =
            dockerClient
                .listContainersCmd()
                .withNameFilter(Collections.singleton(host.name))
                .exec();

        if (!dockerSearch.isEmpty()) {

          statusDTO.setMsg("Environment: " + environment.name + " was already created!");
          statusDTO.setStatus(EnvironmentStatus.ALREDY_CREATED);
          responseDTO.setInfo(statusDTO);

          result = responseDTO;
          isExisted = true;
          break;
        }

         */

        // check existing of host's image locally on the docker
        //
        // Possible problem with pulling too big image.
        // For a small image is ok.
        //
        /*
        List<Image> dockerImages =
            dockerClient
                .listImagesCmd()
                .withImageNameFilter(host.image.name)
                .exec();

        boolean onDonwload = false;
        while (dockerImages.isEmpty()) {
          if (dockerImages.isEmpty() && !onDonwload) {
            // pull image
            dockerClient
                .pullImageCmd(host.image.name)
                .withTag("latest")
                .exec(new PullImageResultCallback());

            onDonwload = true;
          }

          dockerImages =
              dockerClient
                  .listImagesCmd()
                  .withImageNameFilter(host.image.name)
                  .exec();
        }

         */

        // Create volumes
        List<Volume> volumes = new ArrayList<>(host.flavors.size());

        for (Flavor flavor : host.flavors) {
          volumes.add(new Volume(flavor.mountPoint));
        }

        // Bind Ports
        ExposedPort tcp22 = ExposedPort.tcp(22);
        Ports portBindings = new Ports();

        // Check if we already have an exposed port
        List<Container> containersDocker =
            dockerClient
                .listContainersCmd()
                .exec();

        // Check existing of exposed port for the container.
        int bindPort = firstBindPort;

        List<Integer> ports = new ArrayList<>();

        containersDocker.forEach(
            o -> {
              ContainerPort[] p = o.ports;
              for (ContainerPort port : p) {
                ports.add(port.getPublicPort());
              }
            }
        );

        bindPort = getBindPort(ports, bindPort);

        portBindings.bind(tcp22, Ports.Binding.bindPort(bindPort));

        // Create a container
        CreateContainerResponse container =
            dockerClient
                .createContainerCmd(host.image.name)
                //.withName(host.name)
                .withTty(true)
                .withAttachStdout(true)
                .withAttachStdin(true)
                .withExposedPorts(tcp22)
                .withPortBindings(portBindings)
                .withVolumes(volumes)
                .exec();

        dockerClient.startContainerCmd(container.getId()).exec();

        // Create a new host_instance
        HostInstance hostInstance = new HostInstance();
        hostInstance.envInstance = envInstance;
        hostInstance.containerId = container.getId();
        hostInstance.hostId = host.id;

        // Cearching for the special container to save an ip for the host.
        containersDocker =
            dockerClient
                .listContainersCmd()
                .withIdFilter(Collections.singleton(container.getId()))
                .exec();

        containersDocker
            .stream()
            .filter(o -> o.getId().equals(container.getId()))
            .forEach(
                o -> {
                  ContainerNetworkSettings net = o.getNetworkSettings();
                  assert net != null;
                  ContainerNetwork ip = net.getNetworks().get("bridge");

                  hostInstance.url = ip.getIpAddress();
                }
            );

        hostInstance.persistAndFlush();
      }
      if (!isExisted) {

        responseDTO.setId(envInstance.id);
        statusDTO.setMsg("Environment: " + environment.name + " was up");
        statusDTO.setStatus(EnvironmentStatus.RUNNING);
        responseDTO.setInfo(statusDTO);

        envInstance.createdAt = ZonedDateTime.now(ZoneId.of(zone));
        envInstance.userId = environment.userId;
        envInstance.persistAndFlush();
      }
    } catch (Exception exp) {
      // Cannot start the environment instance
      exp.printStackTrace();
      statusDTO.setMsg(exp.getMessage());
      statusDTO.setStatus(EnvironmentStatus.NOT_STARTED);
      responseDTO.setInfo(statusDTO);
      responseDTO.setId(null);

      envInstance.delete();
    }
    if (!isExisted) {
      result = responseDTO;
    }
    return result;
  }

  /**
   * @param ports of all the containers
   * @param bindPort is the initial port from which will be binding
   * @return a new port for binding
   */
  private int getBindPort(List<Integer> ports, int bindPort) {
    if (ports.size() > 0) {

      ports.removeIf(Objects::isNull);

      Collections.sort(ports);

      for (int i = 0; i < ports.size() - 1; i++) {

        if (ports.get(i + 1) > firstBindPort &&
            ports.get(i) >= firstBindPort &&
            ports.get(i + 1) - ports.get(i) >= 2) {
          bindPort = ports.get(i) + 1;
          break;
        }
      }

      if (bindPort == firstBindPort) {
        bindPort = ports.get(ports.size() - 1) + 1;
      }
    }

    return bindPort;
  }

  /**
   * Stop and remove environment's hosts
   *
   * @param envInstance that needs to be stopped and removed
   * @return object that represents how it was going
   */
  @Override
  public EnvironmentResponseDTO stopAndRemove(EnvironmentInstance envInstance) {

    EnvironmentResponseDTO result = null;
    boolean finished = false;

    // Get docker client
    dockerClient = clients.get(envInstance.environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (dockerClient != null) {

      responseDTO.setId(envInstance.id);
      try {

        // Stop and remove all env's hosts
        for (HostInstance host : envInstance.hostInstances) {
          dockerClient.stopContainerCmd(host.containerId).exec();
          dockerClient.removeContainerCmd(host.containerId).exec();
        }

        // Remove client for this environment
        //clients.remove(envInstance.environment.provider.name);

        statusDTO.setMsg("Environment: " + envInstance.environment.name + " was stopped");
        statusDTO.setStatus(EnvironmentStatus.STOPPED);
        responseDTO.setInfo(statusDTO);

        envInstance.hostInstances
            .forEach(PanacheEntityBase::delete);

        envInstance.delete();

        result = responseDTO;
        finished = true;

      } catch (Exception exc) {
        statusDTO.setMsg(exc.getMessage());
        statusDTO.setStatus(EnvironmentStatus.STOPPED);
        responseDTO.setInfo(statusDTO);
      }
    } else {
      responseDTO.setId(envInstance.id);
      statusDTO.setMsg("Client doesn't exist for this environment provider: " +
          envInstance.environment.provider.name +
          ". Before stopping the environment you should run it!");
      statusDTO.setStatus(EnvironmentStatus.UNKONWN);
      responseDTO.setInfo(statusDTO);
    }
    if (!finished) {
      result = responseDTO;
    }
    return result;
  }

  /**
   * Restart the specific environment.
   *
   * @param envInstance represents an environment instance which must be restarted
   * @return object that represents how it was going
   */
  @Override
  public EnvironmentResponseDTO restart(EnvironmentInstance envInstance) {

    // Get docker client
    dockerClient = clients.get(envInstance.environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (dockerClient != null) {

      try {
        for (HostInstance h : envInstance.hostInstances) {
          // Start the docker container by container id
          dockerClient.startContainerCmd(h.containerId).exec();
        }

        statusDTO.setMsg("Environment: " + envInstance.environment.name + " was restarted");
        statusDTO.setStatus(EnvironmentStatus.RUNNING);
        responseDTO.setInfo(statusDTO);

      } catch (Exception exp) {
        statusDTO.setMsg("Environment: " + envInstance.environment.name + " wasn't restarted");
        statusDTO.setStatus(EnvironmentStatus.NOT_STARTED);
        responseDTO.setInfo(statusDTO);
      }
    } else {
      responseDTO.setId(envInstance.id);
      statusDTO.setMsg("Client doesn't exist for this environment provider: " + envInstance.environment.provider.name
          + ". Before restarting the environment you should run it!");
      statusDTO.setStatus(EnvironmentStatus.UNKONWN);
      responseDTO.setInfo(statusDTO);
    }

    responseDTO.setId(envInstance.id);

    return responseDTO;
  }

  /**
   * Pause the specific environment
   *
   * @param envInstance that needs to be paused
   * @return object that represents how it was going
   */
  @Override
  public EnvironmentResponseDTO pause(EnvironmentInstance envInstance) {

    // Get docker client
    dockerClient = clients.get(envInstance.environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (dockerClient != null) {
      responseDTO.setId(envInstance.id);

      try {

        // Pause environment
        envInstance.hostInstances.forEach(
            o ->
                dockerClient
                    .pauseContainerCmd(o.containerId).exec()
        );

        statusDTO.setMsg("Environment: " + envInstance.environment.name + " was paused");
        statusDTO.setStatus(EnvironmentStatus.PAUSED);
        responseDTO.setInfo(statusDTO);

      } catch (Exception exc) {
        statusDTO.setMsg(exc.getMessage());
        statusDTO.setStatus(EnvironmentStatus.NOT_PAUSED);
        responseDTO.setInfo(statusDTO);
      }
    } else {
      responseDTO.setId(envInstance.id);
      statusDTO.setMsg("Client doesn't exist for this environment provider: " + envInstance.environment.provider.name
          + ". Before pausing the environment you should run it!");
      statusDTO.setStatus(EnvironmentStatus.NOT_PAUSED);
      responseDTO.setInfo(statusDTO);
    }

    return responseDTO;
  }

  /**
   * Unpause the specific environment
   *
   * @param envInstance that needs to be unpaused
   * @return object that represents how it was going
   */
  @Override
  public EnvironmentResponseDTO unpause(EnvironmentInstance envInstance) {

    // Get docker client
    dockerClient = clients.get(envInstance.environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    if (dockerClient != null) {
      responseDTO.setId(envInstance.id);

      List<String> hostsId = new ArrayList<>();

      envInstance.hostInstances.forEach(o -> hostsId.add(o.containerId));

      // Get all containers
      List<Container> containers =
          dockerClient
              .listContainersCmd()
              .withIdFilter(hostsId)
              .exec();

      try {
        AtomicBoolean paused = new AtomicBoolean(false);

        // For each puased container we unpause it
        envInstance.hostInstances.forEach(
            o -> {
              containers.forEach(
                  c -> {
                    if (c.getStatus().equals("paused")) {
                      paused.set(true);
                    }
                  }
              );

              if (paused.get()) {
                dockerClient
                    .unpauseContainerCmd(o.containerId)
                    .exec();
              }
            }
        );

        if (paused.get()) {
          statusDTO.setMsg("Environment: " + envInstance.environment.name + " was unpaused");
          statusDTO.setStatus(EnvironmentStatus.UNPAUSED);
          responseDTO.setInfo(statusDTO);
        } else {
          statusDTO.setMsg("Environment: " + envInstance.environment.name + " wasn't paused");
          statusDTO.setStatus(EnvironmentStatus.NOT_UNPAUSED);
          responseDTO.setInfo(statusDTO);
        }
      } catch (Exception exc) {
        statusDTO.setMsg(exc.getMessage());
        statusDTO.setStatus(EnvironmentStatus.NOT_UNPAUSED);
        responseDTO.setInfo(statusDTO);
      }
    } else {
      responseDTO.setId(envInstance.id);
      statusDTO.setMsg("Client doesn't exist for this environment provider: " + envInstance.environment.provider.name
          + ". Before unpausing the environment you should run it!");
      statusDTO.setStatus(EnvironmentStatus.NOT_UNPAUSED);
      responseDTO.setInfo(statusDTO);
    }

    return responseDTO;
  }

  @Override
  public EnvironmentResponseDTO pullImage(Environment environment) {

    // Get docker client
    dockerClient = clients.get(environment.provider.name);

    EnvironmentResponseDTO responseDTO = new EnvironmentResponseDTO();
    EnvironmentStatusDTO statusDTO = new EnvironmentStatusDTO();

    PullImageResultCallback callback = new PullImageResultCallback();

    for (Host h : environment.hosts) {
      System.out.println(h);
      System.out.println(h.image.name);
      try {
        dockerClient
            .pullImageCmd(h.image.name)
            .withRegistry(h.image.path)
            .withTag(h.image.tag)
            .exec(
                new PullImageResultCallback() {
                  @Override
                  public void onNext(PullResponseItem item) {
                    super.onNext(item);
                    statusDTO.setStatus(EnvironmentStatus.IMAGE_PULLED);
                    statusDTO.setMsg("Images were pulled");
                    responseDTO.setInfo(statusDTO);
                  }

                  @Override
                  public void onError(Throwable throwable) {
                    super.onError(throwable);
                    statusDTO.setStatus(EnvironmentStatus.IMAGE_PULLING_EXCEPTION);
                    statusDTO.setMsg("Images weren't puleld");
                    responseDTO.setInfo(statusDTO);
                  }
                })
            .awaitCompletion();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    return responseDTO;
  }

  public Info getClientInfo(String providerName) {
    dockerClient = clients.get(providerName);
    return dockerClient.infoCmd().exec();
  }

  @Override
  public HostsStatusDTO getEnvHostsStatus(EnvironmentInstance envInstance) {

    dockerClient = clients.get(envInstance.environment.provider.name);

    HostsStatusDTO response = new HostsStatusDTO();
    EnvironmentStatusDTO envStatus = new EnvironmentStatusDTO();

    response.setEnvId(envInstance.id);

    if (dockerClient == null) {

      response.setHosts(null);

      envStatus.setStatus(EnvironmentStatus.NO_PROVIDER_CLIENT);

      envStatus.setMsg("Client doesn't exist for this environment provider : " + envInstance.environment.provider.name
          + ". Before getting the environment's status you should run it!");

      response.setEnvStatus(envStatus);

      return response;
    }

    try {

      List<HostStatusDTO> hosts = new ArrayList<>();

      List<String> hostContainersId = new ArrayList<>();

      envInstance.hostInstances.forEach(
          o -> hostContainersId.add(o.containerId)
      );

      // check if we already have an exposed port
      List<Container> containersDocker =
          dockerClient
              .listContainersCmd()
              .withShowAll(true)
              .withIdFilter(hostContainersId)
              .exec();

      // check container's status by env.
      for (Container c : containersDocker) {
        // prepare host status
        HostStatusDTO host = new HostStatusDTO();
        host.setId(c.getId());
        host.setMsg(c.getStatus());

        // check container's state
        switch (c.getState()) {
          case "running":
            host.setStatus(HostStatus.RUNNING);
            envStatus.setStatus(EnvironmentStatus.RUNNING);
            break;
          case "paused":
            host.setStatus(HostStatus.PAUSED);
            envStatus.setStatus(EnvironmentStatus.PAUSED);
            break;
          case "stopped":
            host.setStatus(HostStatus.STOPPED);
            envStatus.setStatus(EnvironmentStatus.STOPPED);
            break;
          case "exited":
            host.setStatus(HostStatus.EXITED);
            envStatus.setStatus(EnvironmentStatus.NOT_STARTED);
            break;
          default:
            host.setStatus(HostStatus.NOT_EXIST);
            envStatus.setStatus(EnvironmentStatus.NOT_STARTED);
            break;
        }
        // add host status to the list of environment's host
        hosts.add(host);
      }
      response.setHosts(hosts);
      envStatus.setMsg("Got status");
    } catch (Exception exp) {
      // Exception appear while trying to get env's hosts status
      response.setHosts(null);
      envStatus.setStatus(EnvironmentStatus.PROVIDER_CLIENT_EXCEPTION);
      envStatus.setMsg("Cannot get hosts' status");
    }
    response.setEnvStatus(envStatus);

    return response;
  }

  /**
   * @param providerName is a provider's name that map specific client
   * @param providerUrl is a provider's url that locates the provider
   */
  public void initClient(String providerName, String providerUrl) {

    DockerClient client = clients.get(providerName);

    if (client == null) {

      DefaultDockerClientConfig config =
          DefaultDockerClientConfig
              .createDefaultConfigBuilder()
              .withDockerTlsVerify(true)
              .withDockerHost(providerUrl + ":" + dockerDeamonPort)
              .build();

      client =
          DockerClientBuilder
              .getInstance(config)
              .withDockerCmdExecFactory(getCmdExecFactory())
              .build();

      clients.put(providerName, client);
    }
  }

  @Override
  public void deleteClient(String providerName) {
    clients.remove(providerName);
  }

  public Map<String, DockerClient> getClients() {
    return clients;
  }

  public DockerClient getClientForProvider(String name) {
    return clients.get(name);
  }

  private DockerCmdExecFactory getCmdExecFactory() {
    return new JerseyDockerCmdExecFactory()
        .withReadTimeout(clientReadTimeout)
        .withConnectTimeout(clientConnectTimeout)
        .withMaxTotalConnections(clientMaxTotalConnections)
        .withMaxPerRouteConnections(clientMaxPerRouteConnections);
  }
}