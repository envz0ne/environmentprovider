-- ----------------------------
-- Table structure for environment_instances
-- ----------------------------
DROP TABLE IF EXISTS "environment_instances";
CREATE TABLE "environment_instances"
(
    "id"     uuid,
    "environment_id" uuid NOT NULL
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Table structure for hosts
-- ----------------------------
DROP TABLE IF EXISTS "host_instances";
CREATE TABLE "host_instances"
(
    "id"             uuid,
    "host_id"        uuid NOT NULL,
    "envinstance_id" uuid,
    "container_id"   varchar(150) NOT NULL,
    "url" varchar(255)
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table environment_instances
-- ----------------------------
ALTER TABLE "environment_instances"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table host_instances
-- ----------------------------
ALTER TABLE "host_instances"
    ADD PRIMARY KEY ("id");

ALTER TABLE "host_instances"
    ADD FOREIGN KEY ("envinstance_id") REFERENCES "environment_instances" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "environment_instances"
    ADD FOREIGN KEY ("environment_id") REFERENCES "environments" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;