-- ----------------------------
-- Table structure for environments
-- ----------------------------
DROP TABLE IF EXISTS "environments";
CREATE TABLE "environments"
(
    "id"          uuid,
    "name"        varchar(100) UNIQUE NOT NULL,
    "provider_id" uuid
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Table structure for flavors
-- ----------------------------
DROP TABLE IF EXISTS "flavors";
CREATE TABLE "flavors"
(
    "id"          uuid,
    "mount_point" varchar(255) NOT NULL,
    "name"        varchar(100) NOT NULL,
    "size"        int4 NOT NULL,
    "host_id"     uuid
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Table structure for hosts
-- ----------------------------
DROP TABLE IF EXISTS "hosts";
CREATE TABLE "hosts"
(
    "id"             uuid,
    "name"           varchar(100) NOT NULL,
    "ram"            int4 NOT NULL,
    "url"            varchar(255) COLLATE "default",
    "vcpu"           int4 NOT NULL,
    "environment_id" uuid,
    "image_id"       uuid
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS "images";
CREATE TABLE "images"
(
    "id"   uuid,
    "name" varchar(100) COLLATE "default",
    "path" varchar(100) COLLATE "default",
    "tag" varchar(50) NOT NULL
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Table structure for providers
-- ----------------------------
DROP TABLE IF EXISTS "providers";
CREATE TABLE "providers"
(
    "id"            uuid,
    "login"         varchar(150) NOT NULL,
    "name"          varchar(150) UNIQUE NOT NULL,
    "password"      varchar(255) NOT NULL,
    "password_salt" varchar(255) NOT NULL,
    "url"           varchar(255) NOT NULL
)
    WITH (OIDS= FALSE)
;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table environments
-- ----------------------------
ALTER TABLE "environments"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table flavors
-- ----------------------------
ALTER TABLE "flavors"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table hosts
-- ----------------------------
ALTER TABLE "hosts"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table images
-- ----------------------------
ALTER TABLE "images"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table providers
-- ----------------------------
ALTER TABLE "providers"
    ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "env_provider_demo"."environments"
-- ----------------------------
ALTER TABLE "environments"
    ADD FOREIGN KEY ("provider_id") REFERENCES "providers" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "env_provider_demo"."flavors"
-- ----------------------------
ALTER TABLE "flavors"
    ADD FOREIGN KEY ("host_id") REFERENCES "hosts" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "env_provider_demo"."hosts"
-- ----------------------------
ALTER TABLE "hosts"
    ADD FOREIGN KEY ("image_id") REFERENCES "images" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "hosts"
    ADD FOREIGN KEY ("environment_id") REFERENCES "environments" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;