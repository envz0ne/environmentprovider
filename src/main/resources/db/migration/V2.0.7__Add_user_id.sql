ALTER TABLE "environments"
ADD COLUMN "user_id" uuid;

ALTER TABLE "environment_instances"
ADD COLUMN "user_id" uuid;

ALTER TABLE "providers"
ADD COLUMN "user_id" uuid;

ALTER TABLE "images"
ADD COLUMN "user_id" uuid;